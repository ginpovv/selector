import os
from PySide6.QtCore import QSize, Qt
from PySide6.QtWidgets import QTableWidgetItem, QTableWidget, QWidget, QStyle, QTableView, QTreeWidget,QTreeWidgetItem, QScroller, QScrollArea, QHBoxLayout, QMenu, QApplication, QCheckBox, QLabel, QMainWindow, QStatusBar, QToolBar
from PySide6.QtGui import QShortcut, QAction, QIcon, QScreen, QWindow, QColor, QPalette, QCloseEvent
from superqt import QRangeSlider
import constant

# resources
manualPath = constant.MANUAL_PATH
appName = constant.APP_NAME
version = constant.VERSION
defW, defH = constant.DEFAULT_WIDTH, constant.DEFAULT_HEIGHT 
minW, minH = constant.MINIMUM_WIDTH, constant.MINIMUM_HEIGHT

# main window
class MainWindow(QMainWindow):
    def __init__(self, app: QApplication):
        super(MainWindow, self).__init__()
        self.app = app
        self.parameters()
        self.menus()
        self.toolbar()
        self.statusbar()
        self.layout()
        self.launch()
            
    def parameters(self):
        """Define base parameters used by the class
        """        
        self.setMinimumSize(QSize(minW, minH)) # BUG: apparently, cannot use setMaximumSize() without completely disabling the maximize button (self.setWindowFlag(Qt.WindowMimizeButtonHint, True) not working) -> issue with DPI awarness?
        self.propertiesWidthRatio = constant.PROPERTIES_PANE_RATIO
        
    def menus(self):            
        """Create the menus of the main window
        """
        fileMenu = self.menuBar().addMenu("&File")
        fileMenuNew = fileMenu.addAction("&New")
        fileMenuNew.triggered.connect(self.aNew)
        fileMenuOpen = fileMenu.addAction("&Open...")
        fileMenuOpen.triggered.connect(self.aOpen)
        fileMenuSave = fileMenu.addAction("&Save")
        fileMenuSave.triggered.connect(self.aSave)
        fileMenuSaveAs = fileMenu.addAction("Save &As...")
        fileMenuSaveAs.triggered.connect(self.aSaveAs)
        fileMenu.addSeparator()
        fileMenuImport = fileMenu.addAction("&Import...")
        fileMenuImport.triggered.connect(self.aImport)
        fileMenuExport = fileMenu.addAction("&Export...")
        fileMenuExport.triggered.connect(self.aExport)
        fileMenu.addSeparator()
        fileMenuSettings = fileMenu.addAction("&Settings...")
        fileMenuSettings.triggered.connect(self.aSettings)
        fileMenu.addSeparator()
        fileMenuExit = fileMenu.addAction("Exit")
        fileMenuExit.triggered.connect(self.aExit)
        
        editMenu = self.menuBar().addMenu("&Edit")
        editMenuUndo = editMenu.addAction("&Undo")
        editMenuUndo.triggered.connect(self.aUndo)
        editMenuRedo = editMenu.addAction("&Redo")
        editMenuRedo.triggered.connect(self.aRedo)
        editMenu.addSeparator()
        editMenuCut = editMenu.addAction("Cu&t")
        editMenuCut.triggered.connect(self.aCut)
        editMenuCopy = editMenu.addAction("&Copy")
        editMenuCopy.triggered.connect(self.aCopy)
        editMenuPaste = editMenu.addAction("&Paste")
        editMenuPaste.triggered.connect(self.aPaste)
        
        selectMenu = self.menuBar().addMenu("&Select")
        selectMenuSelectAll = selectMenu.addAction("Select &All")
        selectMenuSelectAll.triggered.connect(self.aSelectAll)
        selectMenuClearSelection = selectMenu.addAction("&Clear selection")
        selectMenuClearSelection.triggered.connect(self.aClearSelection)
        
        viewMenu = self.menuBar().addMenu("&View")
        viewMenuZoomIn = viewMenu.addAction("Zoom &In")
        viewMenuZoomIn.triggered.connect(self.aZoomIn)
        viewMenuZoomOut = viewMenu.addAction("Zoom &Out")
        viewMenuZoomOut.triggered.connect(self.aZoomOut)
        viewMenuZoomReset = viewMenu.addAction("&Zoom Reset")
        viewMenuZoomReset.triggered.connect(self.aZoomReset)
        viewMenu.addSeparator()
        viewMenuWindowReset = viewMenu.addAction("&Window Reset")
        viewMenuWindowReset.triggered.connect(self.aWindowReset)
        
        helpMenu = self.menuBar().addMenu("&Help")
        helpMenuManual = helpMenu.addAction("&Manual")
        helpMenuManual.triggered.connect(self.aHelpMenuManual)
        helpMenu.addSeparator()
        helpMenuAbout = helpMenu.addAction("&About Selector...")
        helpMenuAbout.triggered.connect(self.aHelpMenuAbout)
        helpMenuAboutQt = helpMenu.addAction("&About Qt...")
        helpMenuAboutQt.triggered.connect(self.aHelpMenuAboutQt)
        
    def toolbar(self):
        """Create the toolbar of the main window
        """        
        # toolbar = QToolBar("toolbar")
        # self.addToolBar(toolbar)
        pass
    
    def statusbar(self):
        """Create the status bar of the main window
        """
        self.statusBar().showMessage("Ready")
        
    def layout(self):
        """Define the layout of the main window
        """
        self.mainLayout = QHBoxLayout()
                
        self.objectsTable = QTableView()
        self.objectsTable.setSelectionBehavior(QTableView.SelectRows)
        self.objectsTable.horizontalHeader().setStretchLastSection(True)
        self.objectsTable.setAlternatingRowColors(True)
        
        self.propertiesTree = QTreeWidget()
        self.propertiesTree.setColumnCount(2)
        self.propertiesTree.setHeaderHidden(True)
                    
        self.mainLayout.addWidget(self.propertiesTree, self.propertiesWidthRatio*100)
        self.mainLayout.addWidget(self.objectsTable, (1 - self.propertiesWidthRatio)*100)
        self.mainWidget = QWidget()
        self.mainWidget.setLayout(self.mainLayout)
        self.setCentralWidget(self.mainWidget)
        
    def closeEvent(self, event: QCloseEvent) -> None:
        """Overload main window closing
        """
        # save settings
        self.saveSettingsToJson()
        return super().closeEvent(event)    
    
    def saveSettingsToJson(self):
        # TODO: Open, edit, save JSON settings
        pass
    
    def aNew(self):
        """Make a new file
        """
        pass
    
    def aOpen(self):
        """Open an existing file
        """
        pass
    
    def aSave(self):
        """Save to current file if existing, or to new file if not
        """
        pass
    
    def aSaveAs(self):
        """Save to new file
        """
        pass
    
    def aImport(self):
        """Import input data
        """
        pass
    
    def aExport(self):
        """Export input data
        """
        pass
    
    def aSettings(self):
        """Settings of the app
        """
        pass
    
    def aExit(self):
        """Exit the app, prompt saving before
        """
        self.app.exit()
        
    def aUndo(self):
        pass
    
    def aRedo(self):
        pass
    
    def aCut(self):
        pass
    
    def aCopy(self):
        pass
    
    def aPaste(self):
        pass
    
    def aSelectAll(self):
        """Select all shown objects
        """
        pass
    
    def aClearSelection(self):
        """Clear current selection
        """
        pass
    
    def aZoomIn(self):
        """Zoom in on the properties and objects widgets
        """
        pass
    
    def aZoomOut(self):
        """Zoom out on the properties and objects widgets
        """
        pass
    
    def aZoomReset(self):
        """Reset zoom level
        """
        pass
    
    def aWindowReset(self):
        """Reset window to default size
        """
        self.resize(defW, defH)
        
    def aHelpMenuManual(self):
        """Open manual file
        """
        os.startfile(manualPath)
        
    def aHelpMenuAbout(self):
        """About window on the app"""
        self.helpMenuAboutText = f"{constant.APP_NAME} v{constant.VERSION}\nBy Gaby AUVRAY\n\nA simple tool written in Python to select items from a spreadsheet simplified database, filtering over several typed properties, all being user-defined with a focus on ease-to-use.\n\nDISCLAIMER: USE AT YOUR OWN RISK, NO WARRANTY, NO LIABILITY.\nLICENSING: See relevant licensing from packaged used, especially PySide6. You shall cite this 'About' message, too. Thanks!"
        self.helpMenuAboutWidget = QLabel()
        self.helpMenuAboutWidget.setWindowTitle("About")
        self.helpMenuAboutWidget.setText(self.helpMenuAboutText)
        self.helpMenuAboutWidget.setWordWrap(True)
        self.helpMenuAboutWidget.setFixedSize(minW, minH)
        self.helpMenuAboutWidget.setAlignment(Qt.AlignmentFlag.AlignLeading)
        self.helpMenuAboutWidget.setStyleSheet(constant.STYLESHEETS["windows"]["about"])
        self.helpMenuAboutWidget.show()
        
    def aHelpMenuAboutQt(self):
        """About window on Qt and PySide
        """
        self.helpMenuAboutQtText = f"Qt 6.6/PySide6\n\nQt is a full development framework with tools designed to streamline the creation of applications and user interfaces for desktop, embedded, and mobile platforms.\nMore: <a href=\"https://www.qt.io\"/>https://www.qt.io</a>\n\nLICENSING: <a href=\"https://www.qt.io/licensing\"/>https://www.qt.io/licensing</a>"
        self.helpMenuAboutQtWidget = QLabel()
        self.helpMenuAboutQtWidget.setWindowTitle("About Qt")
        self.helpMenuAboutQtWidget.setText(self.helpMenuAboutQtText)
        self.helpMenuAboutQtWidget.setWordWrap(True)
        self.helpMenuAboutQtWidget.setFixedSize(minW, minH)
        self.helpMenuAboutQtWidget.setAlignment(Qt.AlignmentFlag.AlignLeading)
        self.helpMenuAboutQtWidget.setStyleSheet(constant.STYLESHEETS["windows"]["about"])
        self.helpMenuAboutQtWidget.show()

    def launch(self):
        self.resize(defW, defH)
        self.setWindowTitle(f"{appName} v{version}")
        self.show()
