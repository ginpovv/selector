import copy
import os
import pandas as pd
import builtins
import constant

# TODO: add a function to reasign defaults from the settings file

class Property:
    def __init__(self, name: str, valueTypeRepr: str, defaultValue):
        self.name = name
        self.valueType = getattr(builtins, valueTypeRepr)
        self.setValue(defaultValue)
    
    def __str__(self):
        return f"Property: name = {self.name}, type = {self.valueType}, value = {self.value}"
        
    def setValue(self, value):
        try:
            self.value = self.valueType(value)
        except:
            self.value = None
            raise ValueError(f"defaultValue {value} could not be casted to valueType {self.valueTypeRepr}, value set to {self.value}.")

class Object:
    def __init__(self, name: str, properties: list[Property]):
        self.name = name
        self.properties: list[Property] = dict(copy.deepcopy(properties))
    
    def __str__(self):
        return f"Object: name = {self.name}, properties = {self.properties}"

class Extract:
    def __init__(self, extractAtInit: bool=True):
        """Args:
            extractAtInit (bool, optional): Extract data from source at init? Defaults to True.
        """
        self.setVars()
        if extractAtInit: self.dataExtraction()

    def setVars(self, filePath: str=constant.FILE_PATH, extensions: list[str]=constant.EXTENSIONS, propertySheetName: str=constant.PROP_SHEET_NAME, objectSheetName: str=constant.OBJ_SHEET_NAME):
        """Set the variables used in this class.

        Args:
            filepath (str, optional): Path to the source file. Defaults to FILE_PATH.
            propertySheetName (str, optional): Sheet of the source that contains properties definitions. Defaults to PROP_SHEET_NAME.
            objectSheetName (str, optional): Sheet of the source that contains objects definitions. Defaults to OBJ_SHEET_NAME.
        """
        self.filePath = filePath
        self.extensions = extensions
        self.propertySheetName = propertySheetName
        self.objectSheetName = objectSheetName
        
        self.fileName, self.fileExtension = os.path.splitext(filePath)
        self.fileExtension = self.fileExtension[1:]
        if self.fileExtension not in extensions:
            raise ValueError("extension "+self.fileExtension+" not supported")
        
        self.propertyDf = pd.read_excel(filePath, sheet_name=propertySheetName)
        self.objectDf = pd.read_excel(filePath, sheet_name=objectSheetName)
        self.propertyDict: dict[str, Property] = {}
        self.objectDict: dict[str, Object] = {}
    
    def dataExtraction(self,
                       propertyNameHeader: str=constant.PROP_NAME_HEADER,
                       propertyTypeHeader: str=constant.PROP_TYPE_HEADER,
                       propertyDefaultValueHeader: str=constant.PROP_DEFAULT_HEADER,
                       objectNameHeader: str=constant.OBJ_NAME_HEADER,
                       objectPropertyHeader: str=constant.OBJ_PROP_HEADER,
                       objectPropertySeparator: str=constant.OBJ_PROP_SEPARATOR,
                       objectPropertyEqualitySign: str=constant.OBJ_PROP_EQUAL,
    ):
        """Extracts the data from source file into pandas dataframes and then into dictionnaries.

        Args:
            propertyNameHeader (str, optional): The header of the names column. Defaults to PROP_NAME_HEADER.
            propertyTypeHeader (str, optional): The header of the types column. Defaults to PROP_TYPE_HEADER.
            propertyDefaultValueHeader (str, optional): The header of the default values column. Defaults to PROP_DEFAULT_HEADER.
        """
        self.propertyNameHeader = propertyNameHeader
        self.propertyTypeHeader = propertyTypeHeader
        self.propertyDefaultValueHeader = propertyDefaultValueHeader
        self.objectNameHeader = objectNameHeader
        self.objectPropertyHeader = objectPropertyHeader
        self.objectPropertySeparator = objectPropertySeparator
        self.objectPropertyEqualitySign = objectPropertyEqualitySign
        
        for i, row in self.propertyDf.iterrows():
            self.propertyDict[row[propertyNameHeader]] = Property(
                row[propertyNameHeader],
                row[propertyTypeHeader],
                row[propertyDefaultValueHeader]
            )

        for i, row in self.objectDf.iterrows():
            props = {}
            propsWithVals: str = row[objectPropertyHeader]
            for pv in propsWithVals.split(objectPropertySeparator):
                p, v = pv.split(objectPropertyEqualitySign)
                prop = self.propertyDict[p]
                prop.setValue(v)
                props[p] = self.propertyDict[p]
            
            self.objectDict[row[objectNameHeader]] = Object(
                row[objectNameHeader],
                props
            )