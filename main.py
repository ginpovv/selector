from PySide6.QtWidgets import QApplication
import sys
import gui
from PySide6.QtWidgets import QTreeWidgetItem
from PySide6.QtCore import Qt
from superqt import QRangeSlider
import data
import constant

# some parameters
verbose = constant.VERBOSE

# create app
app = QApplication(sys.argv)
w = gui.MainWindow(app)

# get settings
# TODO

# extractoin
x = data.Extract() # TODO: feed it settings

# TODO: create functions to add tree items inside gui.py and invoke them here after
for propKey in x.propertyDict:
    item = QTreeWidgetItem(w.propertiesTree)
    item.setText(0, x.propertyDict[propKey].name)
    item.setExpanded(True)
    
    valueType = x.propertyDict[propKey].valueType
    if valueType == str:
        # can't populate now, do it after getting objects
        a = QTreeWidgetItem(item)
        a.setCheckState(0, Qt.CheckState.Unchecked)
    elif valueType == bool:
        pass
    elif (valueType == int or valueType == float):
        slider = QRangeSlider()#self.propertiesTree)
        slider.setOrientation(Qt.Orientation.Horizontal)
        # set slider min and max by iterating over all objects
        for objKey in x.objectDict:
            try:
                slider.setMinimum(min(slider.minimum(), x.objectDict[objKey].properties[propKey].value))
                slider.setMaximum(max(slider.maximum(), x.objectDict[objKey].properties[propKey].value))
            except:
                if verbose: print(f"Property {propKey} not in object {objKey}.")
        w.propertiesTree.setItemWidget(item, 1, slider)

# launch app
app.exec()