VERSION = 0.1
AUTHOR = "Gaby AUVRAY"
APP_NAME = "Selector"
DEFAULT_WIDTH = 800
DEFAULT_HEIGHT = 600
MINIMUM_WIDTH = 360
MINIMUM_HEIGHT = 270
MANUAL_PATH = "res\\manual.txt"
PROPERTIES_PANE_RATIO = 0.3 # Precision of 1/100
VERBOSE = True

EXTENSIONS = {"ods", "xlsx", "xlsm", "xls", "xlm"}
PROP_SHEET_NAME = "properties"
PROP_NAME_HEADER = "property"
PROP_TYPE_HEADER = "type"
PROP_DEFAULT_HEADER = "default_value"
OBJ_SHEET_NAME = "objects"
OBJ_NAME_HEADER = "object"
OBJ_PROP_HEADER = "properties"
OBJ_PROP_SEPARATOR = ";"
OBJ_PROP_EQUAL = "="
FILE_PATH = ".\input.ods"

# styles
STYLESHEETS = {
    "windows": {
        "about": "margin:50px; margin-right:10px; margin-bottom: 10px",
        
    },
    
}